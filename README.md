# CartPole AI

## Requirements

* tensorflow 1.12.0
* tflearn 0.3.2
* gym

## Demo

<p align="center"><img src="https://gitlab.com/sudheer2015/cart-pole-ai-dnn/raw/master/images/CartPoleAI.gif" alt="Video demo"/></p>

## Description

An AI built using reinforcement learning to solve the traditional cart pole problem.

## Installation

> pip install tflearn

## Instructions to run code

### Project directory structure

```
-- data
   -- models
      -- tflearn saved checkpoint files
```

Make a folder data which stores saved models. A pre-trained '477 model' is included with the project. To use the model replace the files in data folder with the files in the model file.

### Run a random game

> python CartPole-v0.py --smart_game no

### Training using previously generated data

> python CartPole-v0.py --smart_game yes --generate_data no

### Training using new data

> python CartPole-v0.py --smart_game yes --generate_data yes

### Inference

> python CartPole-v0.py --smart_game yes --train no

# References

[CartPole tutorial](https://pythonprogramming.net/openai-cartpole-neural-network-example-machine-learning-tutorial/)
 


